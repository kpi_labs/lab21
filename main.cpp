#include <iostream>

using namespace std;

class Animal {
private:
    string m_name;
    string m_race;
    int m_age;
public:
    const string &getName() const {
        return m_name;
    }

    const string &getRace() const {
        return m_race;
    }

    int getAge() const {
        return m_age;
    }

    void setName(const string &t_name) {
        m_name = t_name;
    }

    void setRace(const string &t_race) {
        m_race = t_race;
    }

    void setAge(int t_age) {
        m_age = t_age;
    }

    void printData() {
        cout << "name: " << m_name << " race: " << m_race << " age: " << m_age << endl;
    }
};

int main() {
    Animal animal;
    animal.setAge(8);
    animal.setRace("mammals");
    animal.setName("elephant");
    animal.printData();

    cout << "age: " << animal.getAge() << endl;

    return 0;
}